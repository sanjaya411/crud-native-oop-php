<?php

class DB
{
  protected $db;

  protected function __construct()
  {
    $this->db = new mysqli("localhost", "root", "", "test");
  }

  protected function result_object($query)
  {
    $result = $this->db->query($query);
    if ($result->num_rows > 0) {
      $array = [];
      while ($row = $result->fetch_object()) {
        $array[] = $row;
      }
      return $array;
    } else {
      return FALSE;
    }
  }

  protected function save($table, $input)
  {
    $response = new stdClass();
    if (is_object($input)) {
      $column = "";
      $values = "";
      $delimiter = "";
      foreach ($input as $key => $value) {
        $data = strip_tags(mysqli_real_escape_string($this->db, $value));
        $column .= "$delimiter `$key`";
        $values .= "$delimiter '$data'";
        $delimiter = ",";
      }
      $sql = "INSERT INTO `$table` ($column) VALUES ($values)";
      $query = $this->db->query($sql);
      if ($query) {
        $response->success = TRUE;
        $response->message = "Sukses menambahkan!";
      } else {
        // var_dump($sql);
        $response->success = FALSE;
        $response->message = "Query failed!";
      }
    } else {
      $response->success = FALSE;
      $response->message = "Data tidak valid!";
    }
    return $response;
  }

  protected function get_where($table, $where)
  {
    $response = new stdClass();
    $sql = "SELECT * FROM `$table` WHERE ";
    $delimiter = "";
    foreach ($where as $key => $value) {
      $value = strip_tags(mysqli_real_escape_string($this->db, $value));
      $id = (int)$value;
      if (is_int($id) && $id !== 0) {
        $sql .= "$delimiter `$key` = '$value'";
        $delimiter = "AND";
      } else {
        break;
      }
    }
    $query = $this->db->query($sql);
    if ($query) {
      $response->success = TRUE;
      $response->data = $query->fetch_object();
    } else {
      // var_dump($sql);
      $response->success = FALSE;
      $response->message = "Query failed!";
    }
    return $response;
  }

  protected function update($table, $set)
  {
    $response = new stdClass;
    $values = "";
    $set_where = "";
    $delimiter_where = "";
    $delimiter = "";
    foreach ($set as $key => $value) {
      $data = strip_tags(mysqli_real_escape_string($this->db, $value));
      if (!strpos($key, "id")) {
        $values .= "$delimiter `$key` = '$data'";
        $delimiter = ",";
      } else {
        $set_where .= "$delimiter_where `$key` = $data";
        $delimiter_where = "AND";
      }
    }
    $sql = "UPDATE `$table` SET $values WHERE $set_where";
    $query = $this->db->query($sql);
    // var_dump($sql);
    if ($query) {
      $response->success = TRUE;
      $response->message = "Sukses update data!";
    } else {
      $response->success = FALSE;
      $response->message = $sql;
    }
    return $response;
  }

  protected function delete($table, $where)
  {
    $response = new stdClass;
    $values = "";
    $delimiter = "";
    foreach ($where as $key => $value) {
      $data = strip_tags(mysqli_real_escape_string($this->db, $value));
      $id = (int)$data;
      if (is_int($id) && $id != 0) {
        $values .= "$delimiter `$key` = $data";
        $delimiter = "AND";
      } else {
        break;
      }
    }
    $sql = "DELETE FROM `$table` WHERE $values";
    $query = $this->db->query($sql);
    // var_dump($sql);
    if ($query) {
      $response->success = TRUE;
      $response->message = "Sukses hapus data!";
    } else {
      $response->success = FALSE;
      $response->message = $sql;
    }
    return $response;
  }
}
