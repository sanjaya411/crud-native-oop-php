<?php 
include_once("Database.php");

class Barang extends DB {

  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
  }

  public function get_barang()
  {
    $query = "SELECT * FROM `list_barang`";
    $result = $this->result_object($query);
    if ($result !== FALSE) {
      return $result;
    } else {
      return FALSE;
    }
  }

  public function simpan($input)
  {
    $input->created_date = date("Y-m-d H:i:s");
    $save = $this->save("list_barang", $input);
    return $save;
    // var_dump($input);
  }

  public function detail_barang($id)
  {
    $data = new stdClass();
    $data->barang_id = htmlspecialchars($id);
    $result = $this->get_where("list_barang", $data);
    return $result;
  }

  public function update_barang($input)
  {
    $input->updated_date = date("Y-m-d H:i:s");
    $process = $this->update("list_barang", $input);
    return $process;
  }

  public function delete_barang($id)
  {
    $input = new stdClass();
    $input->barang_id = $id;
    $delete = $this->delete("list_barang", $input);
    return $delete;
  }
}