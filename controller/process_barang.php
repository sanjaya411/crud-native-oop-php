<?php 
session_start();
include_once("../helper/function.php");
include_once("../class/Barang.php");
$barang = new Barang();
$input = (object) $_POST;

if (isset($_GET["barang_id"])) {
  $id = (int) $_GET["barang_id"];
  if ($id != 0) {
    $delete = $barang->delete_barang($id);

    if ($delete->success == TRUE) {
      $_SESSION["status"] = TRUE;
      $_SESSION["message"] = $delete->message;
      header("location:" . URL);
    } else {
      $_SESSION["status"] = FALSE;
      $_SESSION["old"] = $input;
      $_SESSION["message"] = $delete->message;
      header("location:".URL."index.php?page=edit&barang_id=$input->barang_id");
    }
  } else {
    $_SESSION["status"] = FALSE;
    $_SESSION["message"] = "Data tidak valid!";
    header("location:" . URL);
  }
} else if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($input->barang_id)) {
    $update = $barang->update_barang($input);

    if ($update->success == TRUE) {
      $_SESSION["status"] = TRUE;
      $_SESSION["message"] = $update->message;
      header("location:" . URL);
    } else {
      $_SESSION["status"] = FALSE;
      $_SESSION["old"] = $input;
      $_SESSION["message"] = $update->message;
      header("location:".URL."index.php?page=edit&barang_id=$input->barang_id");
    }
  } else {
    $save = $barang->simpan($input);
    if ($save->success === TRUE) {
      $_SESSION["status"] = TRUE;
      $_SESSION["message"] = $save->message;
      header("location:" . URL);
    } else {
      // var_dump($save);
      $_SESSION["status"] = FALSE;
      $_SESSION["old"] = $input;
      $_SESSION["message"] = $save->message;
    }
  }
}