<?php
$old = "";
if (isset($_SESSION["old"])) {
  $old = $_SESSION["old"];
}
?>
<div class="container">
  <div class="card">
    <div class="card-header">
      <h5>Tambah Barang</h5>
    </div>
    <div class="card-body">
      <a class="btn btn-warning btn-sm mb-3" href="<?= URL; ?>">Kembali</a>
      <?php if (isset($_SESSION["status"]) && $_SESSION["status"] === FALSE) { ?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Gagal!</strong> <?= $_SESSION["message"] ?>.
        </div>
      <?php
      session_destroy();
      } ?>
      <form action="<?= URL . "index.php?page=tambah"; ?>" method="POST">
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" class="form-control" name="nama_barang" placeholder="Masukan nama barang..." value="<?= isset($old) && is_object($old) ? $old->nama_barang : ""; ?>">
        </div>
        <div class="form-group">
          <label>Tanggal Masuk</label>
          <input type="date" class="form-control" name="tanggal_masuk" value="<?= isset($old) && is_object($old) ? date("Y-m-d", strtotime($old->tanggal_masuk)) : FALSE; ?>">
        </div>
        <div class="form-group">
          <label>Supplier Barang</label>
          <input type="text" class="form-control" name="supplier" placeholder="Masukan supplier barang..." value="<?= isset($old) && is_object($old) ? $old->supplier : ""; ?>">
        </div>
        <div class="form-group">
          <label>Jumlah Barang</label>
          <input type="text" class="form-control" name="jumlah_barang" placeholder="Masukan jumlah barang..." value="<?= isset($old) && is_object($old) ? $old->jumlah_barang : ""; ?>">
        </div>
        <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
      </form>
    </div>
  </div>
</div>

<?php
// var_dump($_SESSION);
include_once("class/Barang.php");
$barang = new Barang();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $input = (object)$_POST;

  $save = $barang->simpan($input);
  if ($save->success === TRUE) {
    $_SESSION["status"] = TRUE;
    $_SESSION["message"] = $save->message;
    header("location:" . URL);
  } else {
    // var_dump($save);
    $_SESSION["status"] = FALSE;
    $_SESSION["old"] = $input;
    $_SESSION["message"] = $save->message;
  }
}
?>