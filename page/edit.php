<?php
// session_destroy();
include_once("class/Barang.php");
$barang = new Barang();
$old = "";
if (isset($_SESSION["old"])) {
  $old = $_SESSION["old"];
} else if (isset($_GET["barang_id"])) {
  $id = $_GET["barang_id"];
  $data = $barang->detail_barang($id);
  if ($data->success === TRUE) {
    $old = $data->data;
  } else {
    $_SESSION["success"] = FALSE;
    $_SESSION["message"] = $data->message;
    header("location:" . URL);
  }
}
?>
<div class="container">
  <div class="card">
    <div class="card-header">
      <h5>Tambah Barang</h5>
    </div>
    <div class="card-body">
      <a class="btn btn-warning btn-sm mb-3" href="<?= URL; ?>">Kembali</a>
      <?php if (isset($_SESSION["status"]) && $_SESSION["status"] == FALSE) { ?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Gagal!</strong> <?= $_SESSION["message"] ?>.
        </div>
      <?php } session_destroy(); ?>
      <form action="<?= URL . "controller/process_barang.php"; ?>" method="POST">
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" class="form-control" name="nama_barang" placeholder="Masukan nama barang..." value="<?= isset($old) && is_object($old) ? $old->nama_barang : ""; ?>">
          <input type="hidden" name="barang_id" value="<?= $old->barang_id; ?>">
        </div>
        <div class="form-group">
          <label>Tanggal Masuk</label>
          <input type="date" class="form-control" name="tanggal_masuk" value="<?= isset($old) && is_object($old) ? date("Y-m-d", strtotime($old->tanggal_masuk)) : FALSE; ?>">
        </div>
        <div class="form-group">
          <label>Supplier Barang</label>
          <input type="text" class="form-control" name="supplier" placeholder="Masukan supplier barang..." value="<?= isset($old) && is_object($old) ? $old->supplier : ""; ?>">
        </div>
        <div class="form-group">
          <label>Jumlah Barang</label>
          <input type="text" class="form-control" name="jumlah_barang" placeholder="Masukan jumlah barang..." value="<?= isset($old) && is_object($old) ? $old->jumlah_barang : ""; ?>">
        </div>
        <input type="submit" value="Simpan" class="btn btn-primary btn-sm">
      </form>
    </div>
  </div>
</div>