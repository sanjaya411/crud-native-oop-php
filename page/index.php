<?php
include_once("class/Barang.php");;
$barang = new Barang();
?>

<div class="container">
  <div class="card">
    <div class="card-body">
      <h5>CRUD OOP PHP</h5>
      <?php if (isset($_SESSION["status"]) && $_SESSION["status"] === TRUE) { ?>
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Success!</strong> <?= $_SESSION["message"] ?>.
        </div>
      <?php  } else if (isset($_SESSION["status"]) && $_SESSION["status"] == FALSE) { ?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Gagal!</strong> <?= $_SESSION["message"] ?>.
        </div>
      <?php } session_destroy(); ?>
      <a class="btn btn-success btn-sm my-3" href="<?= URL . "index.php?page=tambah"; ?>">Tambah Data</a>
      <table class="table table-bordered table-hover">
        <tr>
          <th>Nama Barang</th>
          <th>Tanggal Masuk</th>
          <th>Supplier</th>
          <th>Jumlah Barang</th>
          <th>Aksi</th>
        </tr>
        <?php
        foreach ($barang->get_barang() as $item) {
          echo "<tr>";
          echo "<td>$item->nama_barang</td>";
          echo "<td>" . date("d M Y", strtotime($item->tanggal_masuk)) . "</td>";
          echo "<td>$item->supplier</td>";
          echo "<td>$item->jumlah_barang pcs</td>";
          echo "<td>";
          echo "<a href='" . URL . "index.php?page=edit&barang_id=$item->barang_id' class='btn btn-primary btn-sm m-1'>Edit</a>";
          echo "<a href='" . URL . "controller/process_barang.php?barang_id=$item->barang_id' class='btn btn-danger m-1 btn-sm'>Hapus</a>";
          echo "</td>";
          echo "</tr>";
        }
        ?>
      </table>
    </div>
  </div>
</div>