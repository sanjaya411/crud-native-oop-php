<?php 
include_once("helper/function.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?= URL."assets/css/bootstrap.min.css"; ?>">
  <title>CRUD OOP</title>
</head>
<body>

  <?php
    if ((isset($_GET["page"]) && $_GET == "") || !isset($_GET["page"])) {
      include_once("page/index.php");
    } else if (isset($_GET["page"]) && $_GET != "") {
      $page = $_GET["page"].".php";
      if (file_exists("page/$page")) {
        include_once("page/$page");
      }
    }
  ?>
  
  <script src="<?= URL."assets/js/jquery.min.js"; ?>"></script>
  <script src="<?= URL."assets/js/bootstrap.min.js"; ?>"></script>
</body>
</html>